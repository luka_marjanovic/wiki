$(".form").find("input, textarea").on("keyup blur focus", function (e) {

	var $this = $(this);
	var label = $this.prev("label");

	if (e.type === "keyup") {
		if ($this.val() === "") {
			label.removeClass("highlight");
		} else {
			label.addClass("active highlight");
		}
	} else if (e.type === "blur") {
		if( $this.val() === "" ) {
			label.removeClass("active highlight"); 
		} else {
			//label.removeClass("highlight");   
		}
	} else if (e.type === "focus") {
		/* 
		if( $this.val() === "" ) {
			label.removeClass("highlight"); 
		} 
		else if( $this.val() !== "" ) {
			label.addClass("highlight");
		}
		*/
		label.addClass("active highlight");
	}

});

$(".tab a").on("click", function (e) {

	e.preventDefault();

	$(this).parent().addClass("active");
	$(this).parent().siblings().removeClass("active");

	target = $(this).attr("href");

	$(".tab-content > div").not(target).hide();

	$(target).fadeIn(600);

});

window.onload = function () {
	if ((document.getElementById("usernameLog")||{}).value != "") $("#usernameLog").focus();
	if ((document.getElementById("passwordLog")||{}).value != "") $("#passwordLog").focus();
	(document.getElementById("usernameReg")||{}).value = "";
	(document.getElementById("passwordReg")||{}).value = "";
}