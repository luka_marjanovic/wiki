var express = module.require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var mysql = require('mysql');
var session = require("express-session");
var cookieParser = require("cookie-parser");
var bcrypt = require("bcryptjs");

var app = express();

// Makes app use "static" folder so server.js can access those files
app.use(express.static(__dirname + "/static"));
app.use("/static", express.static(__dirname + "/static"));

// Set up a session so we can distinguish logged users from ordinary ones
app.use(session({
  secret: "malamut",
  resave: true,
  saveUninitialized: true
}));
app.use(cookieParser());

// App is using bodyParser so server can read body of the requests from the client
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


// Connect to database
var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "Witcher"
});

connection.connect(function(error){
  if(error){
    console.log("Error connectin to db", error);
  } else {
    console.log("Succesfully connected to db !!!");
  }

});

var q = "SELECT * FROM users;";
connection.query(q, function(error, result){
  if(error){
    console.log("MySQL query error", error);
  } else {
    console.log("users: ", result);
    users = result;
  }
});

//connection.end();

app.listen(process.env.PORT || 3000, function () {
  console.log("Listening on port 3000");
});

// Start page, if logged redirect to home, else show signin.html
app.get("/", function (request, response) {
  if (request.session.sessid) response.redirect("/home");
  else response.sendFile(__dirname + "/static/html/register.html");
});

// Home page which is shown to the logged users
app.get("/home", function (request, response) {
  if (request.session.sessid) response.sendFile(__dirname + "/static/html/redirect.html")
  else response.redirect("/");
});

app.get("/send", function (request, response){
  if (request.session.sessid) response.sendFile(__dirname + "/static/html/unos.html"); 
  else response.redirect("/home");
});

app.get("/signout", function (request, response) {
  request.session.destroy();
  response.redirect("/");
});

app.post("/send", function (req, res){
  var username = req.session.sessid;
  var name = req.body.name;
  var desc = req.body.desc;
  var lore = req.body.lore;
  var success = false;

  var user = {"username": username, "Name":name, "Description":desc, "Lore":lore};
    updateUser(username,name,desc,lore);
    success = true;
    console.log("user", user);
    res.json({"success": success});
    

    connection.query('UPDATE users SET Name= ?, Description= ?, Lore= ? WHERE Username= ${username}  ', 
      [name, desc, lore, username] , function(err, result){
      if (err) {
        console.log("Mysql query error", err);
      } else {
        console.log("Users:", result);
      }
    });
});

app.post("/login", function (req, res) {
  var username = req.body.username;
  var password = req.body.password;
  
  // Send query to the database
  connection.query(`SELECT * FROM users WHERE username="${username}";`, function (err, rows, fields) {
    // Error handling
    if (err) {
      console.log("/login POST query ERROR", err);
      return err;
    }

    // If username is undefined then that username doesn't exist in db
    if (typeof rows[0] === "undefined") {
        console.log("Undefined");
    } else {
      // Get encrypted password from db and compare it with the one user has entered
      var dbPassword = rows[0]["Password"];
      var dbSessid = rows[0]["ID"];
      bcrypt.compare(password, dbPassword, function(err, result) {
        // Error handling
        if (err) {
          console.log("/login POST bcrypt compare ERROR", err);
          return err;
        } else if (result == true) { // If result is true, passwords match
          req.session.sessid = dbSessid;
          req.session.username = username;
          res.redirect("/home");
        }
      });
    }
  });
});

app.post("/register", function (req, res) {
  var username = req.body.username;
  var password = req.body.password;
  
  if (username.length <= 25) { // Max number of characters of username in db is 25

    // Create hash that will be saved to db
    var saltRounds = 10;
    bcrypt.hash(password, saltRounds, function(err, hash) {
      // Error handling
      if (err) {
        console.log("/login POST bcrypt ERROR", err);
        res.status(500).json({error: "/login POST bcrypt ERROR"});
      }
        // Send query to database and insert these values
        connection.query(`INSERT INTO users VALUES (null, "${username}", "${hash}","","","")`, function (err, rows, fields) {
          // Error handling
          if (err) {
            console.log("/register POST query ERROR", err);
            return err;
          }else{
          // When all successfully finished, redirect to homepage
          res.redirect("/home");
        }
        });
      });
  } else { // This else will activate if username has more than 25 characters
    response.sendFile(__dirname + "/static/html/error/924.html");
  }
});

app.get("/login", function (req, res){
  res.sendFile(__dirname + "/login.html");
});

app.get("/register", function (req, res){
  res.sendFile(__dirname + "/register.html");
});

app.get("/redirect", function (req, res){
  res.sendFile(__dirname + "/static/html/redirect.html");
});

